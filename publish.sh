#!/bin/bash
set -e
imageName="telegate-publish2pypi"
containerName="telegate-publish2pypi"

# Read Login
echo -n PyPi Login:
read PYPI_LOGIN

# Read Password
echo -n PyPi Password:
read -s PYPI_PASSWD
echo

echo "Building new image.."
docker build --build-arg PYPI_LOGIN="$PYPI_LOGIN" --build-arg PYPI_PASSWD="$PYPI_PASSWD" --tag $imageName -f publish2pypi.Dockerfile .

echo "Stopping and delete old container.."
set +e
docker kill --signal=SIGINT $containerName
sleep 3s
docker rm -f $containerName
set -e

echo "Run new container.."
docker run --rm -it --name $containerName $imageName
echo "Done!"
