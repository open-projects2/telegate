FROM python:3.6-buster as build
LABEL description="Building new image, contains current version of teleGate and make endpoint `/shared` where we can place/link config."
EXPOSE 35001

COPY requirements.txt /app/
RUN pip install --requirement /app/requirements.txt
COPY . /app/

WORKDIR /app/
CMD python -u main.py
