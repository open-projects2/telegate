FROM python:3.6-buster
LABEL description="Building new image, that will build and publish new pip package."
ARG PACKAGE_NAME="teleGate"
ARG PYPI_LOGIN
ARG PYPI_PASSWD
ENV PACKAGE_NAME $PACKAGE_NAME
ENV PYPI_LOGIN $PYPI_LOGIN
ENV PYPI_PASSWD $PYPI_PASSWD

RUN python -m pip install --upgrade twine
# RUN python -m pip install --upgrade setuptools wheel twine

RUN mkdir /packaging
WORKDIR /packaging/
RUN mkdir $PACKAGE_NAME
COPY . $PACKAGE_NAME
RUN mv $PACKAGE_NAME/requirements.txt requirements.txt
RUN mv $PACKAGE_NAME/setup.py setup.py
RUN mv $PACKAGE_NAME/README.md README.md
RUN mv $PACKAGE_NAME/LICENSE LICENSE
RUN rm -f $PACKAGE_NAME/*.Dockerfile
RUN rm -f $PACKAGE_NAME/*.sh
RUN rm -f $PACKAGE_NAME/.gitignore
RUN rm -f $PACKAGE_NAME/.dockerignore
RUN rm -rf $PACKAGE_NAME/.git
RUN rm -rf $PACKAGE_NAME/shared
RUN rm -rf $PACKAGE_NAME/__pycache__

CMD ["sh", "-c", "python setup.py sdist bdist_wheel && python -m twine upload --username ${PYPI_LOGIN} --password ${PYPI_PASSWD} --non-interactive --repository-url https://test.pypi.org/legacy/ dist/* && python -m pip install --index-url https://test.pypi.org/simple/ --no-deps ${PACKAGE_NAME} && python -m twine upload --username ${PYPI_LOGIN} --password ${PYPI_PASSWD} --non-interactive dist/* && python -m pip install ${PACKAGE_NAME} && python -c \"import ${PACKAGE_NAME}\""]

