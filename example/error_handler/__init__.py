import sys, os
p=os.path.normpath(os.path.realpath(__file__)).split(os.path.sep)
sys.path.insert(0, os.path.sep.join(p[:-3]))

from utils import ErrorLogger
import module1

PROJECT='error_handler_example'
TELEGATE_ADDR='http://localhost:35001/api_v1/log'

def tFunc1():
   raise RuntimeError('Test error from main')

def tFunc2():
   module1.tFunc1()

def tFunc3():
   module1.tFunc2()

if __name__ == '__main__':
   logger=ErrorLogger(PROJECT, TELEGATE_ADDR)
   logger.enable_global()

   try:
      tFunc1()
   except Exception:
      logger.warn(sys.exc_info())
   try:
      tFunc2()
   except Exception:
      logger.warn()
   tFunc3()
