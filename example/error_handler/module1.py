import module2

def tFunc1():
   raise RuntimeError('Test error from module1')

def tFunc2():
   return module2.tFunc1()
