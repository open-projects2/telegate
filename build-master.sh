#!/bin/bash
set -e
imageName="telegate:master"
containerName="telegate-master"

echo "Building new image.."
docker build --tag $imageName -f main.Dockerfile .

echo "Stopping and delete old container.."
set +e
docker kill --signal=SIGINT $containerName
sleep 3s
docker rm -f $containerName
set -e

echo "Run new container.."
docker run -d --restart unless-stopped --publish 35001:35001 --volume ~/secrets/teleGate:/shared --name $containerName $imageName
echo "Done!"
