from os import environ
from os.path import isfile, abspath, expanduser

SUBSCRIBERS_FILE = 'shared/subscribers.data'

API_HOST = '0.0.0.0'
API_PORT = 35001
API_BACKLOG_SIZE = 10000

TG_NAME = environ['TG_NAME']
TG_API_ID = environ['TG_API_ID']
TG_API_HASH = environ['TG_API_HASH']
TG_PROXY = None
TG_TOKEN = environ['TG_TOKEN']

SHARED_CONFIG_FILE = 'shared/config.py'

SHARED_CONFIG_FILE = abspath(expanduser(SHARED_CONFIG_FILE))
if isfile(SHARED_CONFIG_FILE):
    import importlib.util

    shared_config_spec = importlib.util.spec_from_file_location('shared_config', SHARED_CONFIG_FILE)
    shared_config_module = importlib.util.module_from_spec(shared_config_spec)
    shared_config_spec.loader.exec_module(shared_config_module)
    for k in dir(shared_config_module):
        if k.startswith('__'): continue
        globals()[k] = getattr(shared_config_module, k)
else:
    print(f'Warning: Shared config not found!')
